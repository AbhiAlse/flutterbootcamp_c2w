//Named argument to fun

void fun({String? str,double? sal}){         //we have to give curley brackets in named arguments
	print("In fun");
	print(str);
	print(sal);
}
void main(){
	print("Start main");
	fun(sal:20.56,str:"Abhi");
	fun();                //it will give null
	fun(str:"Rohan");    //here only one parameter is given other is null
	print("End main");
}



