void main(){

int x;

x=10;
print(x.runtimeType);
print(x);
num y=20.43;          //parent class of int and double
print(y.runtimeType);
print(y);

String name="Abhishek";
print(name.runtimeType);
print(name);

bool flag=false;
print(flag.runtimeType);
print(flag);


var sir="Shashi";
print(sir.runtimeType);
print(sir);

//sir=10;    error one value assigned to var variable it cant be changed
print(sir.runtimeType);
print(sir);

dynamic a=10;           
print(a.runtimeType);
print(a);

a="Shashi";   //dynamic can change their value runtime
print(a.runtimeType);
print(a);


}
