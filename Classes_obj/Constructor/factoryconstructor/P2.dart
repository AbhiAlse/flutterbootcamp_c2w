class Demo{
	static Demo obj=new Demo();
	Demo Demo(){
		print("In cons");
		return obj;                     //constructors dont have a return type but factory constructors have return type
	}
}
void main(){
	Demo obj1=new Demo();
	Demo obj2=new Demo();
	
	print(obj1.hashCode);
	print(obj2.hashCode);
}
