abstract class Devloper{
		factory Devloper(String Devtype){	
			if(Devtype=="Backend")
				return Backend();
			
			if(Devtype=="Frontend")
				return Frontend();

			if(Devtype=="Mobile")
				return Mobile();
	
			else
				return Other();
		}
		void Devlang();
}

class Backend extends Devloper{
		void Devlang(){
			print("Springboot/NodeJS");
		}
}

class Frontend extends Devloper{
		void Devlang(){
			print("Html/css/js");
		}
}
class Mobile extends Devloper{
		void Devlang(){
			print("Flutter");
		}
}
class Other extends Devloper{
		void Devlang(){
			print("DevOps/Tester");
		}
}


void main(){
	Devloper obj1= new Devloper("Frontend");
	obj1.Devlang();
	
	Devloper obj2=new Devloper("Backend");
	obj2.Devlang();
	
	Devloper obj3=new Devloper("Mobile");
	obj3.Devlang();
	
	Devloper obj4=new Devloper("Other");
	obj4.Devlang();

}
