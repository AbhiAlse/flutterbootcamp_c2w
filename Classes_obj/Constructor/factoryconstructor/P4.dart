class Backend{
	String? str;
	Backend._code(String lang){
		if(lang=="Javascript")
			this.str="NodeJS";
		else if(lang=="Java")
			this.str="Springboot";
		else
			this.str="NodeJS/SpringBoot";
	
	}
	factory Backend(String lang){
		return Backend._code(lang);    //returning the object of self class
	}
}
