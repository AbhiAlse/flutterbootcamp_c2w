// Same here also named parameter is used 
class Company{
	
	int ? empCount;
	String ?compName;

	Company({this.empCount,this.compName="Deollite"});
	
	void compInfo(){
		
		print(this.empCount);
		print(compName);
	}
}

void main(){
		
	Company obj1=Company(empCount:100,compName:"Veritas");
	obj1.compInfo();
	
	Company obj2=Company(compName:"Pubmatic",empCount:200);
	obj2.compInfo();

}

		



