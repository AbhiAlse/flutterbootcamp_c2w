// Here in construtor we have used named parameter's they are initilized by curley brackets 
// Also if we not pass the values so it will consider the default values 
// And if we want to change the parameter so we should give it by specific name passing through arguments 

class Company{

	int ?x;
	String ?str;
	Company(this.x,{this.str="Core2web"});
	
	void compInfo(){
		print(x);
		print(str);

	}

}

void main(){

	Company obj1 = Company(100);
	obj1.compInfo();
	Company obj2 = Company(200,"Binecaps"); //error we should give it by specific name 
	obj2.compInfo();

}



