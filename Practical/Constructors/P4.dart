/*
Here the dafault parameter construtor is used so that if anyargument is not passed it will used that default one 
Default parameter is declared in Square Brackets = []
*/


class Company{
	int empCount;
	String compName;
	
	Company(this.empCount,[this.compName='Binecaps']);
	
	void compInfo(){
		print(empCount);
		print(compName);
	}
}

void main(){
	
	Company obj1=new Company(100);
	obj1.compInfo();
	Company obj2=new Company(100,"Core2web");
	obj2.compInfo();
}

		



