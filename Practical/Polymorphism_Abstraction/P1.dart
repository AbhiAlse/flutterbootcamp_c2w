/*
Here the compName method is overriden by child class so it will called nearest method that is his own method 
*/

class Company {
	void companyName() {
		print('Google');
	}
}
class Employee extends Company {
	
	
	void companyName() {
		print('Apple');
	}
}

void main() {
	Company obj = Employee();
	obj.companyName();
}


