
abstract class Test{
	void build();
}

class Test2 extends Test{
	@override
	void build(){
		super.build();  //the method is abstract in parent class so we cant call it 
	}
}


