//Here fun is not defined for Demo1 so it will give error at compile time that reference is of parent but object is done of child so that method should be present in parent class Also

class Demo1{
	int x;
	Demo1(this.x);
	
	
}

class Demo2 extends Demo1 {
	Demo2(super.x);
	void fun(){
		print(x);
		
	}
}

void main(){
	Demo1 obj1=Demo2(10);
	
	obj1.fun();
}


