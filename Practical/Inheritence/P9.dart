/*
In Dart, when a child class inherits from a parent class, it does not inherit static variables or methods. Each class, including the child class, maintains its own set of static members.
*/

class Test {
	int? x;

	static int y=20;
	//Named Construtor
	Test.initX(this.x);
	
	static void changeY(){
		y=30;
	}
}
class Test2 extends Test{
		//calling superclass named construtor an passing the value
	Test2(int x):super.initX(x);
}	

void main(){
	Test2 obj=Test2(40);
	//child class will not have those static variables and methods
	Test2.changeY();
	print(Test2.y);
}




