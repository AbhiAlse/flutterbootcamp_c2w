
class Parent {
	Parent(){
		print("In Parent Constructcor");
	}
}

class Child extends Parent {
	Child(){            // Child ():super() {}           == This is the way to call the parent constructor
		super();     //This cant make a call to the parent contrutor
		print("In child Constructor");
	}
}

void main(){
	Child obj=Child();
}



