// Test2 class extends Test class so x and y variable of Test class comes but test2 also has its own X variable this.y will be pointing throung the superclass 

class Test{
	int x=30;
	int y=30;
	
}

class Test2 extends Test {
	int x;

	Test2(this.x);
	
	void gun(){
		this.x=8;
		this.y=19;
	}
	void fun(){
		print(super.x);
		print(super.y);
	}
}

void main(){
	Test2 obj = Test2(10);
	obj.gun();
	obj.fun();

}
	





