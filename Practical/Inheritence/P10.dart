/*
We cannot make the object of abstarct class because it can have 0% to 100% abstarct methods so we cant make object it is also incomplete class
But we can make only when there is factory constructor beacuse it return its chilg object or self object
*/

abstract class Demo1 {

	factory Demo1(){
		print("In Demo1");
		return Demo2();
	}
}
class Demo2 implements Demo1{
	Demo2(){
		print("Demo2");
	}
}

void main(){
	Demo1 obj=new Demo1();
}


